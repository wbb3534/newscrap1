package com.seop.newscrap1.service;

import com.seop.newscrap1.model.NewItem;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

@Service
public class ScrapService {
    private Document getFullHtml() throws IOException {
        String url = "https://www.maxmovie.com/";
        Connection connection = Jsoup.connect(url);

        Document document = connection.get();
        return document;
    }

    private List<Element> parseHtml(Document document) {
        Elements elements = document.getElementsByClass("ItemCard__Content-sc-1faiai4-0");
        List<Element> result = new LinkedList<>();
        for (Element item : elements) {
            Elements lis = item.getElementsByTag("li");
            for (Element item2 : lis) {
                result.add(item2);
            }
        }
        return result;
    }

    private List<NewItem> makeResult(List<Element> list) {
        List<NewItem> result = new LinkedList<>();
        for (Element item : list) {
            Elements checkTag = item.getElementsByClass("middleContents");
            if (checkTag.size() == 0) {
                String imgUrl = item.getElementsByClass("imgBox").get(1).attr("style").replace("background-image:url(", "").replace(");", "").replace("background-size:cover", "");
                String title = item.getElementsByClass("textBox").get(0).getElementsByTag("h4").get(0).text();
                String contents = item.getElementsByClass("textBox").get(0).getElementsByTag("p").get(0).text();
                String anchorName = item.getElementsByClass("textBox").get(0).getElementsByTag("a").get(1).text();

                NewItem addItem = new NewItem();
                addItem.setImgUrl(imgUrl);
                addItem.setTitle(title);
                addItem.setContents(contents);
                addItem.setAnchorName(anchorName);

                result.add(addItem);
            }
        }
        return result;
    }
    //위에메서드들을 관리하는 메서드
    public List<NewItem> run() throws IOException {
        Document document = getFullHtml();
        List<Element> elements = parseHtml(document);
        List<NewItem> result = makeResult(elements);
        return result;
    }
}
