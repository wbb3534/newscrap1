package com.seop.newscrap1.controller;

import com.seop.newscrap1.model.NewItem;
import com.seop.newscrap1.service.ScrapService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/scrap")
@RequiredArgsConstructor
public class ScrapController {

    private final ScrapService scrapService;

    @GetMapping("/html")
    public List<NewItem> getHtml() throws IOException {
        List<NewItem> result = scrapService.run();
        return result;
    }

}
