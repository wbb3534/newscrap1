package com.seop.newscrap1.controller;

import com.seop.newscrap1.model.ConsumerRequest;
import com.seop.newscrap1.model.MoneyChangeRequest;
import com.seop.newscrap1.service.ConsumerService;
import com.seop.newscrap1.service.MoneyService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/bank")
@RequiredArgsConstructor
public class MoneyController {
    private final MoneyService moneyService;
    private final ConsumerService consumerService;
    @PostMapping("/deposit")
    public String deposit(@RequestBody MoneyChangeRequest request) {
        String result = moneyService.convertMoney(request.getMoney());
        return result + " 입금되었습니다. 고객님";
    }

    @PostMapping("/withdrawal")
    public String withdrawal(@RequestBody ConsumerRequest request) {
        String result = consumerService.consumerName(request.getName());
        return result +" 출금되었습니다";
    }
}
