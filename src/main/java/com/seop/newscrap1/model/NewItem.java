package com.seop.newscrap1.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NewItem {
    private String imgUrl;
    private String title;
    private String contents;
    private String anchorName;
}
