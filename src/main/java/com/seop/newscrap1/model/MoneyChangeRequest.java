package com.seop.newscrap1.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MoneyChangeRequest {
    private Integer money;
}
