package com.seop.newscrap1.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConsumerRequest {
    private String name;
}
