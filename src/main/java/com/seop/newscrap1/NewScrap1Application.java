package com.seop.newscrap1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NewScrap1Application {

	public static void main(String[] args) {
		SpringApplication.run(NewScrap1Application.class, args);
	}

}
